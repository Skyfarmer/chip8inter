//
// Created by simon on 5/16/18.
//

#ifndef CHIP8INTER_ABSTRACT_CHIP8_H
#define CHIP8INTER_ABSTRACT_CHIP8_H

#include <fstream>
#include <algorithm>
#include "../Info/utilities.h"

namespace Core {
    class abstract_chip8 {
    protected:
        template<typename Container>
         void load_rom(const std::string &rom, Container &data, std::size_t offset = 0) {
            //Load binary
            std::fstream file(rom, std::fstream::binary | std::fstream::in);
            if (!file.is_open()) {
                throw std::runtime_error(rom + ": No such file!");
            }

            for(auto value = data.begin() + offset; value != data.end(); value++) {
                if(!file.eof()) {
                    char temp;
                    file.read(&temp, 1);

                    *value = static_cast<uint8>(temp);
                } else {
                    break;
                }
            }

            if(!file.eof()) {
                throw std::runtime_error("Container too small!");
            }
        }
    public:
        abstract_chip8() = default;
        virtual ~abstract_chip8() = default;

        DELETE_COPY_CONSTRUCTOR(abstract_chip8);
        DELETE_COPY_ASSIGNMENT(abstract_chip8);

        DEFAULT_MOVE_CONSTRUCTOR(abstract_chip8);
        DEFAULT_MOVE_ASSIGNMENT(abstract_chip8);

        virtual void run() = 0;

    };
}  // namespace Core

#endif //CHIP8INTER_ABSTRACT_CHIP8_H
