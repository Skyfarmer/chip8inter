//
// Created by simon on 5/12/18.
//
#include "interpreter.h"

#include <algorithm>
#include <fstream>
#include <random>
#include <limits>
#include <bitset>
#include <optional>
#include <thread>

namespace Core {

    uint16 interpreter::get_random() const {
        std::random_device r;
        std::default_random_engine e(r());
        std::uniform_int_distribution<uint16> uni(0, 255);

        return uni(e);

    }


    void interpreter::execute() {
        //Fetch opcode
        m_current_op = m_memory[m_pc] << 8u | m_memory[m_pc + 1];

        auto x{(m_current_op & get_0f00) >> 8u};
        auto y{(m_current_op & get_00f0) >> 4u};

        //Decode opcode
        switch (m_current_op & get_f000) {
            case 0x0000:
                switch (m_current_op) {
                    case 0x00EE:
                        //Returns from a subroutine.
                        m_pc = m_stack[--m_sp];
                        break;
                    case 0x00E0:
                        //Clears the screen
                        m_gfx.fill(false);
                        m_callbacks.screen_clear();
                        break;
                    default:
                        throw std::runtime_error("Fatal error: This opcode of type 0 is not supported!");
                }
                m_pc += 2;
                break;
            case 0x1000:
                m_pc = m_current_op & get_0fff;
                break;
            case 0x2000:
                //Calls subroutine at NNN.
                m_stack[m_sp++] = m_pc;
                m_pc = m_current_op & get_0fff;
                break;
            case 0x3000:
                //Skips the next instruction if VX equals NN. (Usually the next instruction is a jump to skip a code block)
                if (m_registers[x] == (m_current_op & get_00ff)) {
                    m_pc += 4;
                } else {
                    m_pc += 2;
                }
                break;
            case 0x4000:
                //Skips the next instruction if VX doesn't equal NN. (Usually the next instruction is a jump to skip a code block)
                if (m_registers[x] != (m_current_op & get_00ff)) {
                    m_pc += 4;
                } else {
                    m_pc += 2;
                }
                break;
            case 0x5000:
                //Skips the next instruction if VX equals VY. (Usually the next instruction is a jump to skip a code block)
                if (m_registers[x] == m_registers[y]) {
                    m_pc += 4;
                } else {
                    m_pc += 2;
                }
                break;
            case 0x6000:
                //Sets VX to NN.
                m_registers[x] = static_cast<uint8>(m_current_op);
                m_pc += 2;
                break;
            case 0x7000:
                //Adds NN to VX. (Carry flag is not changed)
                m_registers[x] += static_cast<uint8>(m_current_op);
                m_pc += 2;
                break;
            case 0x8000:
                //8-Series
            {
                switch (m_current_op & get_f00f) {
                    case 0x8000:
                        //Sets VX to the value of VY.
                        m_registers[x] = m_registers[y];
                        break;
                    case 0x8001:
                        //Sets VX to VX or VY. (Bitwise OR operation)
                        m_registers[x] |= m_registers[y];
                        break;
                    case 0x8002:
                        //Sets VX to VX and VY. (Bitwise AND operation)
                        m_registers[x] &= m_registers[y];
                        break;
                    case 0x8003:
                        //Sets VX to VX xor VY.
                        m_registers[x] ^= m_registers[y];
                        break;
                    case 0x8004: {
                        //Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't.
                        uint32 value{static_cast<uint32>(m_registers[x]) + m_registers[y]};
                        if (value > std::numeric_limits<uint8>::max()) {
                            m_registers[0xF] = 1;
                        } else {
                            m_registers[0xF] = 0;
                        }
                        m_registers[x] = static_cast<uint8>(value);
                        break;
                    }
                    case 0x8005: {
                        //VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
                        if (m_registers[x] > m_registers[y]) {
                            m_registers[0xF] = 1;
                        } else {
                            m_registers[0xF] = 0;
                        }

                        m_registers[x] -= m_registers[y];
                        break;
                    }
                    case 0x8006: {
                        // If the least-significant bit of Vx is 1, then VF is set to 1, otherwise 0. Then Vx is divided by 2.
                        std::bitset<sizeof(uint8)*8> temp_bitset{m_registers[x]};
                        m_registers[0xF] = static_cast<uint8>(temp_bitset[0]);
                        m_registers[x] /= 2;
                        break;
                    }
                    case 0x8007: {
                        //Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
                        if (m_registers[y] > m_registers[x]) {
                            m_registers[0xF] = 1;
                        } else {
                            m_registers[0xF] = 0;
                        }

                        m_registers[x] = m_registers[y] - m_registers[x];
                        break;
                    }
                    case 0x800E: {
                        //Shifts VY left by one and copies the result to VX. VF is set to the value of the most significant bit of VY before the shift.
                        std::bitset<sizeof(uint8)*8> temp_bitset{m_registers[x]};
                        if (temp_bitset[7]) {
                            m_registers[0xF] = 1;
                        } else {
                            m_registers[0xF] = 0;
                        }
                        m_registers[x] *= 2;
                        break;
                    }
                    default:
                        throw std::runtime_error("Fatal error: This opcode of type 8 is not supported!");
                }
            }
                m_pc += 2;
                break;
            case 0x9000:
                //Skips the next instruction if VX doesn't equal VY. (Usually the next instruction is a jump to skip a code block)
                if (m_registers[x] != m_registers[y]) {
                    m_pc += 4;
                } else {
                    m_pc += 2;
                }
                break;
            case 0xA000:
                //Sets I to the address NNN.
                m_index_register = m_current_op & get_0fff;
                m_pc += 2;
                break;
            case 0xB000:
                throw std::runtime_error("Jumps not yet supported!");
                break;
            case 0xC000:
                //Sets VX to the result of a bitwise and operation on a random number (Typically: 0 to 255) and NN.
                m_registers[x] = static_cast<uint8>(get_random()) & static_cast<uint8>(m_current_op);
                m_pc += 2;
                break;
            case 0xD000: {
                //Drawing a sprite
                uint16 x_cord{m_registers[x]};
                uint16 y_cord{m_registers[y]};
                uint16 height = m_current_op & get_000f;
                uint16 pixel{0};

                //Reset for collision detection
                m_registers[0xF] = 0;
                for (uint16 yline = 0; yline < height; yline++) {
                    pixel = m_memory[m_index_register + yline];
                    for (uint16 xline = 0; xline < 8; xline++) {
                        if ((pixel & (0x80u >> xline)) != 0) {
                            if (m_gfx[(x_cord + xline + ((y_cord + yline) * 64)) % m_gfx.size()]) {
                                m_registers[0xF] = 1;
                            }
                            m_gfx[(x_cord + xline + ((y_cord + yline) * 64)) % m_gfx.size()] ^= true;
                        }
                    }
                }
                m_callbacks.screen_draw(m_gfx);
                m_pc += 2;
            }
                break;
            case 0xE000:
                //E-Series
            {
                switch (m_current_op & get_f0ff) {
                    case 0xE09E:
                        //Skips the next instruction if the key stored in VX is pressed. (Usually the next instruction is a jump to skip a code block)
                        if (m_keys[m_registers[x]] != 0) {
                            m_keys[m_registers[x]] = 0;
                            m_pc += 2;
                        }
                        break;
                    case 0xE0A1:
                        //Skips the next instruction if the key stored in VX isn't pressed. (Usually the next instruction is a jump to skip a code block)
                        if (m_keys[m_registers[x]] == 0) {
                            m_pc += 2;
                        } else {
                            m_keys[m_registers[x]] = 0;
                        }
                        break;
                    default:
                        throw std::runtime_error("Fatal error: This opcode of type e is not supported");
                }
            }
                m_pc += 2;
                break;
            case 0xF000:
                //F-Series
            {
                switch (m_current_op & get_f0ff) {
                    case 0xF007:
                        //Sets VX to the value of the delay timer.
                        m_registers[x] = m_delay_timer;
                        break;
                    case 0xF00A: {
                        //A key press is awaited, and then stored in VX. (Blocking Operation. All instruction halted until next key event)
                        std::optional<Keys> value;
                        do {
                            value = m_callbacks.input_fetch_key(true);
                        } while (!value);
                        m_registers[x] = static_cast<uint8>(value.value());
                        break;
                    }
                    case 0xF015:
                        //Sets the delay timer to VX.
                        m_delay_timer = m_registers[x];
                        break;
                    case 0xF018:
                        //Sets the sound timer to VX.
                        m_sound_timer = m_registers[x];
                        break;
                    case 0xF01E:
                        //Adds VX to I
                        m_index_register += m_registers[x];
                        break;
                    case 0xF029:
                        //Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal)
                        //are represented by a 4x5 font.
                        m_index_register = static_cast<uint16>(m_registers[x] * 5);
                        break;
                    case 0xF033: {
                        /*Stores the binary-coded decimal representation of VX, with the most significant of three digits
                         * at the address in I, the middle digit at I plus 1, and the least significant digit at I plus 2.
                         * (In other words, take the decimal representation of VX, place the hundreds digit in memory at
                         * location in I, the tens digit at location I+1, and the ones digit at location I+2.)
                         */
                        auto temp{m_registers[x]};
                        for (int i = 2; i >= 0; i--) {
                            m_memory[m_index_register + i]
                                    = static_cast<uint8>(temp % 10);
                            temp /= 10;
                        }
                        break;
                    }
                    case 0xF055:
                        //Stores V0 to VX (including VX) in memory starting at address I. I is increased by 1 for each value written.
                        for (auto i = 0; i <= x; i++) {
                            m_memory[m_index_register + i] = m_registers[i];
                        }
                        break;
                    case 0xF065:
                        //Fills V0 to VX (including VX) with values from memory starting at address I. I is increased by
                        // 1 for each value written.
                        for (auto i = 0; i <= x; i++) {
                            m_registers[i] = m_memory[m_index_register + i];
                        }
                        break;
                    default:
                        throw std::runtime_error("Fatal error: This opcode of type f is not supported");
                }
            }
                m_pc += 2;
                break;
            default:
                throw std::runtime_error("Opcode unimplemented: " + std::to_string(m_current_op));
        }

        //Update delay timer
        if (m_delay_timer > 0) {
            m_delay_timer--;
        }
        //Update sound timer
        if (m_sound_timer > 0) {
            m_callbacks.play_sound_effect();
            m_sound_timer--;
        }

        //Update keyboard
        auto input{m_callbacks.input_fetch_key(false)};
        if (input.has_value()) {
            auto key{static_cast<uint8>(input.value())};

            m_keys[key] = 1;
        }

        //Update emulator counter
        if(m_pause_interval == m_pause_counter) {
            std::this_thread::sleep_for(std::chrono::milliseconds(m_pause_length));
            m_pause_counter = 0;
        } else {
            m_pause_counter++;
        }
    }

    interpreter::interpreter(const std::string &rom, callbacks &&display) :
            m_current_op{0},
            m_memory{},
            m_registers{},
            m_index_register{0},
            m_pc{0x200},
            m_stack{},
            m_sp{0},
            m_gfx{},
            m_delay_timer{0},
            m_sound_timer{0},
            m_callbacks(std::move(display)),
            m_keys{},
            m_pause_length{0},
            m_pause_interval{0},
            m_pause_counter{0} {

        load_rom(rom, m_memory, 0x200);
        //Fill array with font values
        {
            const static std::array<uint8, 80> fonts{
                    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
                    0x20, 0x60, 0x20, 0x20, 0x70, // 1
                    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
                    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
                    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
                    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
                    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
                    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
                    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
                    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
                    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
                    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
                    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
                    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
                    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
                    0xF0, 0x80, 0xF0, 0x80, 0x80  // F
            };

            std::copy(fonts.begin(), fonts.end(), m_memory.begin());
        }
    }

    void interpreter::run() {
        while(!m_callbacks.input_exit()) {
            execute();
        }
    }

    void interpreter::set_pause_length(uint16 length) noexcept {
        m_pause_length = length;
    }

    void interpreter::set_pause_interval(uint16 interval) noexcept {
        m_pause_interval = interval;
    }

} //namespace Core