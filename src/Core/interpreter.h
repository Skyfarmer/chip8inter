//
// Created by simon on 4/19/18.
//

#ifndef CHIP8INTER_CHIP8_H
#define CHIP8INTER_CHIP8_H

#include <array>
#include <string>

#include "../Info/keyboard.h"
#include "../Info/utilities.h"
#include "../Interfaces/callbacks.h"
#include "abstract_chip8.h"

namespace Core {
    class interpreter : public abstract_chip8 {
    private:
        uint16 m_current_op;
        std::array<uint8, 4096> m_memory;
        std::array<uint8, 16> m_registers;

        uint16 m_index_register;
        uint16 m_pc;

        std::array<uint16, 16> m_stack;
        uint16 m_sp;

        //Graphics hardware
        std::array<bool, 64 * 32> m_gfx;
        uint8 m_delay_timer;
        uint8 m_sound_timer;

        //Used for interacting with other subsystems
        callbacks m_callbacks;

        //Keyboard
        std::array<uint8, 16> m_keys;

        uint16 get_random() const;

        //Emulation speed
        uint16 m_pause_length;
        uint16 m_pause_interval;
        uint16 m_pause_counter;

    public:
        explicit interpreter(const std::string &rom, callbacks &&display);

        void execute();
        void run() final;

        //Specify for how long the emulator (in ms) should sleep
        void set_pause_length(uint16 length) noexcept;
        //Specify after how many cycles the emulator should pause
        void set_pause_interval(uint16 interval) noexcept;
    };
} //namespace Core

#endif //CHIP8INTER_CHIP8_H
