//
// Created by simon on 5/16/18.
//

#include "llvm_recompiler.h"
#include "../Info/utilities.h"

#include <fstream>
#include <iostream>

namespace Core {

    llvm_recompiler::llvm_recompiler(const std::string &rom, callbacks &&display) :
            m_module(std::make_unique<llvm::Module>("Chip-8 JIT", m_context)),
            m_callbacks(std::move(display)) {
        load_rom(rom, m_data);
    }

    void llvm_recompiler::run() {
        std::cout << "Error: The llvm recompiler is not yet working.\n";
    }

} //namespace Core