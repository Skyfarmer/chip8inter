//
// Created by simon on 5/16/18.
//

#ifndef CHIP8INTER_LLVM_RECOMPILER_H
#define CHIP8INTER_LLVM_RECOMPILER_H

#include "abstract_chip8.h"
#include "../Interfaces/callbacks.h"

#include <llvm/IR/IRBuilder.h>
#include <memory>
#include <vector>


namespace Core {
    class llvm_recompiler : public abstract_chip8 {
    private:
        llvm::LLVMContext m_context;
        llvm::IRBuilder<> m_builder{m_context};
        std::unique_ptr<llvm::Module> m_module;

        callbacks m_callbacks;
        std::vector<uint8> m_data{std::vector<uint8>(4096)};

    public:
        explicit llvm_recompiler(const std::string &rom, callbacks &&display);

        void run() final;
    };
} //namespace Core

#endif //CHIP8INTER_LLVM_RECOMPILER_H
