//
// Created by simon on 6/1/18.
//

#ifndef CHIP8INTER_ABSTRACT_DISPLAY_H
#define CHIP8INTER_ABSTRACT_DISPLAY_H

#include "../Info/utilities.h"

namespace Display {
    class abstract_display {
    public:
        virtual ~abstract_display() = default;
        abstract_display() = default;

        DELETE_COPY_CONSTRUCTOR(abstract_display);
        DELETE_COPY_ASSIGNMENT(abstract_display);

        DEFAULT_MOVE_CONSTRUCTOR(abstract_display);
        DEFAULT_MOVE_ASSIGNMENT(abstract_display);
    };
} //namespace Display

#endif //CHIP8INTER_ABSTRACT_DISPLAY_H
