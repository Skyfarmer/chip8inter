//
// Created by simon on 4/26/18.
//

#include <iostream>
#include <curses.h>
#include "curses_display.h"

namespace Display {

    curses_display::curses_display() noexcept {
        setlocale(LC_ALL, "");

        initscr();
        cbreak();
        noecho();

        clear();
    }

    bool curses_display::escape() const noexcept {
        return m_escape;
    }


    void curses_display::draw(const std::array<bool, 64 * 32> &gfx) noexcept {
        //Clear screen
        clear();

        for (std::size_t i = 0; i < gfx.size(); i++) {
            int x{static_cast<int>(i % 64)};
            int y{static_cast<int>(i / 64)};

            if (gfx[i]) {
                mvprintw(y, x, "\u2588");
            }
        }

        refresh();
    }

    std::optional<Keys> curses_display::fetch_key(bool blocking) noexcept {
        if (blocking) {
            nodelay(stdscr, FALSE);
        } else {
            nodelay(stdscr, TRUE);
        }

        int ch = getch();

        switch (ch) {
            case '1':
                return Keys::One;
            case '2':
                return Keys::Two;
            case '3':
                return Keys::Three;
            case '4':
                return Keys::Letter_C;
            case 'q':
                return Keys::Four;
            case 'w':
                return Keys::Five;
            case 'e':
                return Keys::Six;
            case 'r':
                return Keys::Letter_D;
            case 'a':
                return Keys::Seven;
            case 's':
                return Keys::Eight;
            case 'd':
                return Keys::Nine;
            case 'f':
                return Keys::Letter_E;
            case 'y':
                return Keys::Letter_A;
            case 'x':
                return Keys::Zero;
            case 'c':
                return Keys::Letter_B;
            case 'v':
                return Keys::Letter_F;
            case 27:
                m_escape = true;
                return fetch_key(blocking);
            default:
                return std::nullopt;
        }
    }

    curses_display::~curses_display() {
        endwin();
    }

    void curses_display::clear_screen() noexcept {
        clear();
        refresh();
    }
} //namespace Display

