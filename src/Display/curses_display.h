//
// Created by simon on 4/26/18.
//

#ifndef CHIP8INTER_CURSES_DISPLAY_H
#define CHIP8INTER_CURSES_DISPLAY_H

#include <array>
#include <optional>

#include "../Info/keyboard.h"
#include "abstract_display.h"

namespace Display {
    class curses_display : public abstract_display {
    private:
        bool m_escape{false};
    public:
        explicit curses_display() noexcept;

        curses_display(const curses_display &) = delete;
        curses_display operator=(const curses_display &) = delete;

        curses_display(curses_display &&) = default;
        curses_display &operator=(curses_display &&) = default;

        ~curses_display() final;

        bool escape() const noexcept;

        //Chip-8 interface
        void draw(const std::array<bool, 64 * 32> &gfx) noexcept;

        std::optional<Keys> fetch_key(bool blocking = false) noexcept;

        void clear_screen() noexcept;
    };
} //namespace Display

#endif //CHIP8INTER_CURSES_DISPLAY_H
