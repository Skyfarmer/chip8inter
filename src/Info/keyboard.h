//
// Created by simon on 4/28/18.
//

#ifndef CHIP8INTER_KEYBOARD_H
#define CHIP8INTER_KEYBOARD_H

enum class Keys {
    One, Two, Three, Letter_C,
    Four, Five, Six, Letter_D,
    Seven, Eight, Nine, Letter_E,
    Letter_A, Zero, Letter_B, Letter_F
};

#endif //CHIP8INTER_KEYBOARD_H
