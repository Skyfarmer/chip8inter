//
// Created by simon on 5/12/18.
//

#ifndef CHIP8INTER_UTILITIES_H
#define CHIP8INTER_UTILITIES_H

#include <cstdint>

//Renaming typedefs
using int8 = std::int8_t;
using int16 = std::int16_t;
using int32 = std::int32_t;
using int64 = std::int64_t;

using uint8 = std::uint8_t;
using uint16 = std::uint16_t;
using uint32 = std::uint32_t;
using uin64 = std::uint64_t;

constexpr uint16 get_f000{0xF000};
constexpr uint16 get_0fff{0x0FFF};
constexpr uint16 get_0f00{0x0F00};
constexpr uint16 get_00f0{0x00F0};
constexpr uint16 get_000f{0x000F};
constexpr uint16 get_f0ff{0xF0FF};
constexpr uint16 get_f00f{0xF00F};
constexpr uint16 get_00ff{0x00FF};

#define DEFAULT_COPY_CONSTRUCTOR(x) x(const x&) = default
#define DEFAULT_COPY_ASSIGNMENT(x) x &operator=(const x&) = default
#define DELETE_COPY_CONSTRUCTOR(x) x(const x&) = delete
#define DELETE_COPY_ASSIGNMENT(x) x& operator=(const x&) = delete

#define DEFAULT_MOVE_CONSTRUCTOR(x) x(x&&) = default //NOLINT
#define DEFAULT_MOVE_ASSIGNMENT(x) x &operator=(x&&) = default //NOLINT
#define DELETE_MOVE_CONSTRUCTOR(x) x((x)&&) = delete
#define DELETE_MOVE_ASSIGNMENT(x) x &operator=((x)&&) = delete //NOLINT

#endif //CHIP8INTER_UTILITIES_H
