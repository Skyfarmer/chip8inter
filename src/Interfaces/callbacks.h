//
// Created by simon on 5/12/18.
//

#ifndef CHIP8INTER_CALLBACKS_H
#define CHIP8INTER_CALLBACKS_H

#include <functional>
#include <array>
#include <optional>

#include "../Info/keyboard.h"

namespace Core {
    struct callbacks {
        //Video
        std::function<void(const std::array<bool, 64 * 32> &)> screen_draw;
        std::function<void()> screen_clear;

        //Input
        std::function<std::optional<Keys>(bool)> input_fetch_key;
        std::function<bool()> input_exit;

        //Audio
        std::function<void()> play_sound_effect;
    };
} //namespace Core
#endif //CHIP8INTER_CALLBACKS_H
