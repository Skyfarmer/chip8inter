//
// Created by simon on 6/1/18.
//

#include "misc.h"

std::istream &operator>>(std::istream &in, Engines &engine) {
    std::string token;
    in >> token;

    if(token == "interpreter") {
        engine = Engines::interpreter;
    } else if (token == "llvm") {
        engine = Engines::llvm;
    } else {
        in.setstate(std::ios_base::failbit);
    }

    return in;
}

std::ostream &operator<<(std::ostream &out, const Engines engine) {
    switch(engine) {
        case Engines::interpreter:
            out << "interpreter";
            break;
        case Engines::llvm:
            out << "llvm";
            break;
    }

    return out;
}

std::istream &operator>>(std::istream &in, Interfaces &presentation) {
    std::string token;
    in >> token;

    if(token == "qt") {
        presentation = Interfaces::qt;
    } else if (token == "ncurses") {
        presentation = Interfaces::ncurses;
    } else {
        in.setstate(std::ios_base::failbit);
    }

    return in;
}

std::ostream &operator<<(std::ostream &out, const Interfaces engine) {
    switch(engine) {
        case Interfaces::qt:
            out << "qt";
            break;
        case Interfaces::ncurses:
            out << "ncurses";
            break;
    }

    return out;
}