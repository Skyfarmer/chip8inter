//
// Created by simon on 6/1/18.
//

#ifndef CHIP8INTER_MISC_H
#define CHIP8INTER_MISC_H

#include <iostream>

enum class Engines {
    interpreter,
    llvm
};

extern std::istream &operator>>(std::istream &in, Engines &engine);
extern std::ostream &operator<<(std::ostream &out, Engines engine);

enum class Interfaces {
    qt,
    ncurses
};

extern std::istream &operator>>(std::istream &in, Interfaces &presentation);
extern std::ostream &operator<<(std::ostream &out, Interfaces engine);

#endif //CHIP8INTER_MISC_H
