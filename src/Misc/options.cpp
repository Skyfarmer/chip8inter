//
// Created by simon on 6/1/18.
//

#include "options.h"
#include <boost/program_options.hpp>

#include <iostream>

namespace Misc {

    std::optional<options> options::handle_command_line(int argc, char **argv) {
        namespace po = boost::program_options;
        po::options_description fixed_options("Allowed fixed_options");
        fixed_options.add_options()
                ("help,h", "Show this message")
                ("execution-mode,e", po::value<Engines>()->default_value(Engines::interpreter),
                 "Select cpu engine: Either llvm or interpreter")
                ("presentation-mode,p", po::value<Interfaces>()->default_value(Interfaces::ncurses),
                 "Select presentation backend: Either qt or ncurses")
                ("benchmark", po::value<bool>()->default_value(false),
                 "Benchmark the emulator")
                ("rom-file", po::value<std::string>(), "Path to rom file");

        po::positional_options_description positional_options;
        positional_options.add("rom-file", -1);

        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(fixed_options)
                          .positional(positional_options).run(), vm);

        po::notify(vm);

        //Handle help
        if (vm.count("help") > 0) {
            std::cout << fixed_options << '\n';
            return std::nullopt;
        }

        //Handle case where rom-file is empty
        if (vm.count("rom-file") == 0) {
            std::cerr << "No path specified\n";
            return std::nullopt;
        }

        options option(vm["benchmark"].as<bool>(),
                       vm["presentation-mode"].as<Interfaces>(),
                       vm["execution-mode"].as<Engines>(),
                       vm["rom-file"].as<std::string>());

        return option;
    }
}//namespace Misc