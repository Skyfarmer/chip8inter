//
// Created by simon on 6/1/18.
//

#ifndef OPTIONS_H
#define OPTIONS_H

#include "misc.h"

#include <optional>
#include <string>

namespace Misc {
    struct options {
        bool m_benchmark;
        Interfaces m_interface;
        Engines m_engine;
        std::string m_rom_path;

        static std::optional<options> handle_command_line(int argc, char **argv);

    private:
        options(bool benchmark,
                Interfaces interface,
                Engines engine,
                std::string rom) :
                m_benchmark(benchmark),
                m_interface(interface),
                m_engine(engine),
                m_rom_path(std::move(rom))
        {}
    };

} //namespace Misc

#endif //OPTIONS_H
