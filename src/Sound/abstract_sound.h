//
// Created by simon on 6/1/18.
//

#ifndef CHIP8INTER_ABSTRACT_SOUND_H
#define CHIP8INTER_ABSTRACT_SOUND_H

#include "../Info/utilities.h"

namespace Sound {
    class abstract_sound {
    public:
        virtual ~abstract_sound() = default;
        abstract_sound() = default;

        DELETE_COPY_CONSTRUCTOR(abstract_sound);
        DELETE_COPY_ASSIGNMENT(abstract_sound);

        DEFAULT_MOVE_CONSTRUCTOR(abstract_sound);
        DEFAULT_MOVE_ASSIGNMENT(abstract_sound);
    };
} //namespace Sound

#endif //CHIP8INTER_ABSTRACT_SOUND_H
