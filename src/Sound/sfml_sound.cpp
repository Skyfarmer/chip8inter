//
// Created by simon on 5/12/18.
//

#include "sfml_sound.h"
#include "ding.h"

#include <SFML/Audio.hpp>

namespace Sound {

    sfml_sound::sfml_sound() {
        if (!m_buffer.loadFromMemory(&Ding_wav, Ding_wav_len)) {
            throw std::runtime_error("Error while loading sound file from memory");
        }

        m_ding.setBuffer(m_buffer);
    }

    void sfml_sound::play_ding() {
        m_ding.play();
    }

    void sfml_sound::add_music(const std::string &path) {
        if (!m_music.openFromFile(path)) {
            throw std::runtime_error(path + ": No such file!");
        }

        m_music.play();
        m_music.setLoop(true);
    }
} //namespace Sound