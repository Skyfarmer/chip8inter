//
// Created by simon on 5/12/18.
//

#ifndef CHIP8INTER_SFML_SOUND_H
#define CHIP8INTER_SFML_SOUND_H

#include <string>
#include <SFML/Audio.hpp>

#include "abstract_sound.h"

namespace Sound {
    class sfml_sound : public abstract_sound {
    private:
        sf::SoundBuffer m_buffer;
        sf::Sound m_ding;

        sf::Music m_music;
    public:
        explicit sfml_sound();

        void play_ding();

        void add_music(const std::string &path);
    };
} //namespace Sound

#endif //CHIP8INTER_SFML_SOUND_H
