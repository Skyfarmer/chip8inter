#include <iostream>
#include <chrono>
#include <tuple>

#include "Core/interpreter.h"
#include "Core/llvm_recompiler.h"
#include "Core/abstract_chip8.h"

#include "Display/abstract_display.h"
#include "Display/curses_display.h"

#include "Sound/abstract_sound.h"
#include "Sound/sfml_sound.h"

#include "Interfaces/callbacks.h"
#include "Misc/misc.h"
#include "Misc/options.h"

struct session {
    std::unique_ptr<Sound::abstract_sound> sound;
    std::unique_ptr<Display::abstract_display> display;
    std::unique_ptr<Core::abstract_chip8> core;
};

std::tuple<Core::callbacks,
        std::unique_ptr<Sound::abstract_sound>,
        std::unique_ptr<Display::abstract_display>>

setup_interface(const Interfaces interface) {
    Core::callbacks calls;
    std::unique_ptr<Sound::abstract_sound> ab_sound;
    std::unique_ptr<Display::abstract_display> ab_display;

    switch (interface) {
        case Interfaces::qt:
            throw std::runtime_error("The Qt interface is not yet supported!");
        case Interfaces::ncurses: {
            auto display = std::make_unique<Display::curses_display>();
            auto sound = std::make_unique<Sound::sfml_sound>();

            calls.screen_draw = std::bind(&Display::curses_display::draw, display.get(), std::placeholders::_1);
            calls.screen_clear = std::bind(&Display::curses_display::clear_screen, display.get());
            calls.input_fetch_key = std::bind(&Display::curses_display::fetch_key, display.get(),
                                              std::placeholders::_1);
            calls.input_exit = std::bind(&Display::curses_display::escape, display.get());
            calls.play_sound_effect = std::bind(&Sound::sfml_sound::play_ding, sound.get());

            ab_display = std::move(display);
            ab_sound = std::move(sound);
        }
            break;
    }

    return std::make_tuple(std::move(calls), std::move(ab_sound), std::move(ab_display));
}

std::unique_ptr<Core::abstract_chip8> setup_core(const Engines engine,
                                                 const std::string &rom_path,
                                                 Core::callbacks &&calls) {
    std::unique_ptr<Core::abstract_chip8> core;
    switch (engine) {
        case Engines::llvm:
            core = std::make_unique<Core::llvm_recompiler>(rom_path, std::move(calls));
            break;
        case Engines::interpreter: {
            auto temp = std::make_unique<Core::interpreter>(rom_path, std::move(calls));
            //Should be user modifiable
            temp->set_pause_length(40);
            temp->set_pause_interval(20);
            core = std::move(temp);
            break;
        }
    }

    return core;
}

int main(int argc, char **argv) {

    auto options = Misc::options::handle_command_line(argc, argv);
    if(options.has_value()) {
        auto opt = options.value();
        session sess;
        //setup session
        {
            auto [calls, sound, display] = setup_interface(opt.m_interface);
            sess.core = setup_core(opt.m_engine, opt.m_rom_path, std::move(calls));
            sess.sound = std::move(sound);
            sess.display = std::move(display);
        }

        auto start = std::chrono::system_clock::now();
        sess.core->run();
        auto end = std::chrono::system_clock::now();

        sess.display.release();

        if (opt.m_benchmark) {
            std::cout << "Execution took " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
                      << " milliseconds.\n";
        }
    }
    return 0;
}
